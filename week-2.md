# Description
In this guide you will find the solution for the project of week 2. The project consists on deploying the infrastructure in Microsoft Azure needed for the WeightTracker application and deploying the application into the infrastructure. 




# Overview
- The architecture of the WeightTracker infrastructure consits on:
  - Virtual Network
    - Network Security Groups ( web-server and db-server )
    - Private subnet ( associated with the db-server security group )
    - Public subnet ( associated with the web-server security group )
  - Virtual machines
    - Web Server deployed in public subnet
    - Database server deployed in private subnet

After deploying the above infrastructure we are then going to the deploy the WeightTracker Application into it.

# Pre-requisites
- Microsoft Azure Account
- Weight Tracker Application code
- Free Okta developer account ( For authentication, app dependency )

# Procedure

### Step 1: Create Resource Group
First we are going to create a resource group for the WeightTracker resources.

### Step 2: Create Virtual Network
After creating the Resource group we are going to deploy a virtual network into it.

### Step 3: Create a Network Security Group for the public subnet
We are going to create a Network Security Group that allows access to the virtual machines from the internet.

### Step 4: Create a Network Security Group for the private subnet
We are going to create a Network Security Group that allows access to the virtual machines from the internet.

### Step 5: Create Subnets
We are going to create a public subnet ( associated with the "public" network security group ) and a private subnet ( associated with the "private" network security group )

### Step 6: Deploy web-server vm into the public subnet
After setting up a network that has a public and private subnet we are going to deploy our virtual machines. We will start by creating a Windows Server 2019 instance into the public subnet

### Step 7: Deploy db-server vm into the public subnet
We also need to deploy a Virtual Machine for the Database but it will be in the private subnet and only accesible by the public subnet instances.

### Step 8: Configure the application in each of the servers ( See Week 1 solution )
