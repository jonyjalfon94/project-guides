# Description
In this guide you will find the solution for the project of week 1. The project consists on deploying a nodejs application and a postgress database to Linux and Windows servers.
For this project we need the following:

- 1 Ubuntu 20.04 Server
- 2 Windows 2019 Server
- Weight Tracker Application code

# Overview
- WeightTracker is a website that helps you track your weight and see your progress with time. In this project, we will deploy the web application on two Windows servers and a Postgress database in a Linux server.

# Pre-requisites
- Internet access in all the machines
- Git ( Not showing installation as it is out of the scope for this week )
- Free Okta developer account ( For authentication, app dependency )

# Procedure

## Db Server:

### Step 1: Access the Ubuntu server
Access the server with it's external ip and the identity (.pem) file.
```
ssh <user>@<server-external-ip> -i pem/file/path
```
### Step 2: Install PostgreSQL
Use the apt package manager to install PostgreSQL.
```
sudo  apt update
sudo  apt  install postgresql postgresql-contrib
```
### Step 3: Add a password to the postgres user
When PostgreSQL is installed it comes with a "postgres" user by default. We are going to create a password for this user so that we can access the database from outside of the server.
- Run the **psql** command from the `postgres` user account:
```
sudo -u postgres psql postgres
```
- Set the password:
```
\password postgres
```
- Enter a password
- Close psql
```
\q
```
### Step 4: Allow external acces to server ( User-Password Authentication )
PostgreSQL is not accessible to the outside by default. We are going to change the configuration files to enable access from outside of the server.
- Configure postrgresql.conf
```
sudo vim /etc/postgresql/12/main/postgresql.conf
```
Replace 
```
listen_addresses = 'localhost'
```
With:
```
listen_addresses = '*'
```
- Configure pg_hba.conf
```
sudo vim /etc/postgresql/12/main/pg_hba.conf
```
Add the following lines to the end of the file:
```
host    all             all              0.0.0.0/0              md5
host    all             all              ::/0                   md5
```
- Restart the postgres server
```
sudo systemctl restart postgresql
```

## Web Servers:

### Step 1: Access the Windows servers
Use RDP ( Remote Desktop Protocol ) to access the windows server.
- RDP into the server
### Step 2: Install Required dependencies
After accessing the server we need to install a few things needed to download, build, and run the application.
 - Use Chocolatey or simply download and install using the browser the following:
   - git
   - Nodejs
### Step 3: Get application source code
The application source code is in github. We are going to use the git CLI to download the application source code to the windows server.
```
git clone https://github.com/jonyjalfon94/node-weight-tracker.git
```
### Step 4: Download application dependencies
Using npm ( Node Package Manager ) install the application's dependencies.
```
cd node-weight-tracker
npm install
```
### Step 5: Add application to Okta and enable self service registration
This application uses "Okta" for the management of users. We are going to create a free okta developer account and register the application.

- Create free account in https://developer.okta.com/

![](./images/week1/okta-sign-up.jpg)

- After you create your account, click the Applications link at the top, and then click Add Application.

![](./images/week1/okta-add-application.jpg)

- Next, choose to create a Web Application and click Next.

![](./images/week1/okta-add-web-application.jpg)

- Enter a name for your application, such as Node Weight Tracker. Update the Logout redirect URIs to http://localhost:8080/logout. Then, click Done to finish creating the application.

![](./images/week1/okta-add-app-settings.jpg)

- In the Okta application console, click on your new application's General tab, and find near the bottom of the page a section titled "Client Credentials." ( Used in the next step ) Copy the Client ID and Client secret values and paste them into your .env file to replace {yourClientId} and {yourClientSecret}, respectively. 

![](./images/week1/okta-client-credentials.jpg)

- Click on the Dashboard link at the top. Find your Org URL to the right of the page.( Used in the next step ) Copy this URL and paste it into your .env file to replace {yourOrgUrl}.

![](./images/week1/okta-org-url.jpg)

- To allow other people to sign up for an account in your application, you need to enable the self-service registration feature. Click on the Users menu and select Registration.

![](./images/week1/okta-user-registration.jpg)

Next, click the Edit button. Change Self-service registration to Enabled. Make sure the Show "Sign Up" link" is checked.

![](./images/week1/okta-user-registration-settings.jpg)

Finally, click the Save button at the bottom of the form.

### Step 6:  Copy .env.sample as .env and modify it for your environment
In the .env file there are variables used throghout the code that we need to change to our environment. 
```
# Host configuration
PORT=8080
HOST=localhost
NODE_ENV=production
HOST_URL=http://localhost:8080
COOKIE_ENCRYPT_PWD=superAwesomePasswordStringThatIsAtLeast32CharactersLong!

# Okta configuration
OKTA_ORG_URL=<okta-domain-url>
OKTA_CLIENT_ID=<okta-client-id>
OKTA_CLIENT_SECRET=<octa-client-secret>

# Postgres configuration
PGHOST=<postgres-host>
PGUSERNAME=<postgres-user>
PGDATABASE=<postgres-db>
PGPASSWORD=<postgres-password>
PGPORT=5432
```
### Step 5: Initialize database
In the source code of the application there is a file called initdb.js that initializes the database and in the package.json file we configure npm to run this script with the "npm run initdb" in the scripts section.
```
npm run initdb
```
### Step 6: Download pm2 to run the application as a service
We are going to install pm2 ( globally "-g"). pm2 is a tool that help to manage and keep the application online.
```
npm install -g pm2
```
### Step 7: Add prod script to package.json
We are going add another script for npm that starts the application using pm2.
```
"scripts": {
	"dev": "nodemon --watch src -e css,ejs,js src/index.js",
	"prod": "pm2 start src/index.js",# <--( This one )--
	"initdb": "node tools/initdb.js",
	"lint": "eslint --fix ./",
	"test": "echo \"Error: no test specified\" && exit 1"
},
```
### Step 5: Run the application as a service using pm2
Run the script that we just added to the package.json file to run the application as a service.
```
npm run prod
```
### Step 6: Try accesing the application locally
- Go to your browser and see if you can access the application in http://localhost:8080.

```
http://<server-external-ip>:8080
```

### Step 7: Configure IIS as a reverse proxy to enable access from the outside to the application
The nodejs application is running as a service and reachable through localhost:8080 from the windows server but is not yet reachable from the outside. We are going to configure IIS to get traffic from the outside and redirect it to our application.

Add step by step explanation with screenshots like in here:
https://dev.to/petereysermans/hosting-a-node-js-application-on-windows-with-iis-as-reverse-proxy-397b
